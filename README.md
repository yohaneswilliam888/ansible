# Ansible

This is my ansible script

Please run the script at requirment folder first at your master and client.
1. Master.sh
	this file will install ansible on your master server
2. Client.sh
	This file will install python and allow ssh port

Ansible using push configuration from master server.
ansible has 3 items:
- Master server
- Inventory
- Playbook

Ansible file is yaml. Rules of YAML:
- YAML files should end in .yaml
- YAML is case sensitive
- YAML doesn't allow the use of tabs. Spaces are used instead as tabs are not universally supported.


Ansible Terminology
-------------------
- Control Node: Machine with ansible installed
- Managed node: Machines that you manage with Ansible
- Inventory: List of managed nodes, default file location is /etc/ansible/hosts
- Moduls: Unit of code that ansible executes
- Task: Unit of Action in Ansible
- Playbooks: Ordered list of task


Hosts Configuration
-------------------

By default hosts file located on /etc/ansible/hosts. You can change it to other path that you want.  
Please follow below steps:  
**sudo nano /etc/ansible/ansible.cfg**
- change inventory path
inventory	= /home/william/ansible/hosts

You can setup hosts file linke below:
- Multiple hosts:
[Dev]  
dev[1-4].lab  

- Spesific user
[Dev]  
william-lab1 ansible_user=william  

- Custom ssh port 78
[Dev]  
william-lab1:78  

- Variable for Host
[dev:vars]  
ansible_user=william  
http_port=80  

- SuperGroup for hosts
[new_env:children]  
dev  
prod  

- SuperGroup Vars
[new_env:vars]  
ftp_server=10.10.10.1

You can use below command to check your ansible playbook:  
**ansible-playbook installApache2.yaml --syntax-check**

please visit:
docs.ansible.com
